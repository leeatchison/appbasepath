
# Usage

## Setup(appName string)

Call this first. It sets the application name for this application.

For example Setup("www.leeatchison.com/go/appbasepath")

## GoBasePath() string

Returns the absolute path name to the GO base directly (as defined by GOPATH, or defaults).

For example: "/users/lee/go"

## Path(relativeDir string) string

Returns the absolute pathname to the specified file/directory, assuming the path passed in is relative to GOPATH.

For example: Path("./here/there") will return "/users/lee/go/here/there".

## AppPath(appName string) string

Returns the absolute pathname to the specified application home directory, assuming the appName is passed in.

For example AppPath() will return "/users/lee/go/src/www.leeatchison.com/go/appbasepath" assuming Setup(...) was called as above.

## AppFullPath(appRelativePath string) string

Returns the absolute pathname to the specified directory within the application directory.

For example AppFullPath("./templates") will return "/users/lee/go/src/www.leeatchison.com/go/appbasepath/templates" assuming Setup(...) ws called as above.

# Testing

```bash
make test
```
