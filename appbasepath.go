package appbasepath

//
// Usage:
//
// 		Setup(appName string)
// 			Call this first. It sets the application name for this application.
// 			For example:
//     			Setup("www.leeatchison.com/go/appbasepath")
//
// 		GoBasePath() string
// 			Returns the absolute path name to the GO base directly (as defined by GOPATH, or defaults).
// 			For example:
//     			the call: GoBasePath()
//     			will return: "/users/lee/go"
//
// 		Path(relativeDir string) string
// 			Returns the absolute pathname to the specified file/directory, assuming the path passed in is relative to GOPATH.
// 			For example:
//     			the call: Path("./here/there")
//     			will return: "/users/lee/go/here/there"
//
// 		AppPath(appName string) string
// 			Returns the absolute pathname to the specified application home directory, assuming the appName is passed in.
//			For example:
//     			assuming this call: Setup("www.leeatchison.com/go/appbasepath")
//     			then the call: AppPath()
//     			will return: "/users/lee/go/src/www.leeatchison.com/go/appbasepath"
//
// 		AppFullPath(appRelativePath string) string
// 			Returns the absolute pathname to the specified directory within the application directory.
// 			For example:
//     			assuming this call: Setup("www.leeatchison.com/go/appbasepath")
//     			then the call: AppFullPath("./templates")
//     			will return: "/users/lee/go/src/www.leeatchison.com/go/appbasepath/templates"
//

import (
	"go/build"
	"os"
	"path/filepath"
)

var goBasePath string
var appName string

// Setup - Setup base path.
// **Call this first.** It sets the application name for this application.
// For example:
//     Setup("www.leeatchison.com/go/appbasepath")
func Setup(setAppName string) {
	appName = setAppName
}

// GoBasePath - Returns the absolute path name to the GO base directly
// (as defined by GOPATH or defaults).
// For example:
//     the call: GoBasePath()
//     will return: "/users/lee/go"
func GoBasePath() string {
	if goBasePath == "" {
		goBasePath = os.Getenv("GOPATH")
		if goBasePath == "" {
			goBasePath = build.Default.GOPATH
		}
	}
	return goBasePath
}

// Path - Returns the absolute pathname to the specified file/directory,
// assuming the path passed in is relative to GOPATH.
// For example:
//     the call: Path("./here/there")
//     will return: "/users/lee/go/here/there"
func Path(relativeDir string) string {
	return filepath.Clean(GoBasePath() + "/" + relativeDir)
}

// AppPath - Returns the absolute pathname to the specified application
// home directory, assuming the appName is passed in.
// For example:
//     assuming this call: Setup("www.leeatchison.com/go/appbasepath")
//     then the call: AppPath()
//     will return: "/users/lee/go/src/www.leeatchison.com/go/appbasepath"
func AppPath() string {
	return filepath.Clean(GoBasePath() + "/src/" + appName)
}

// AppFullPath - Returns the absolute pathanme to the specified directory
// within the application directory.
// For example:
//     assuming this call: Setup("www.leeatchison.com/go/appbasepath")
//     then the call: AppFullPath("./templates")
//     will return: "/users/lee/go/src/www.leeatchison.com/go/appbasepath/templates"
func AppFullPath(appRelativePath string) string {
	return filepath.Clean(GoBasePath() + "/src/" + appName + "/" + appRelativePath)
}
