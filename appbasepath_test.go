package appbasepath

import (
	"testing"
)

func assertTest(t *testing.T, a interface{}, b interface{}) {
	if a != b {
		t.Error("Expected", a, "got", b)
	}
}

func TestGoBasePath(t *testing.T) {
	Setup("testapp")
	assertTest(t, GoBasePath(), GoBasePath())
}
func TestPath(t *testing.T) {
	Setup("testapp")
	goBase := GoBasePath()
	assertTest(t, Path("test1"), goBase+"/test1")
	assertTest(t, Path("/test2"), goBase+"/test2")
	assertTest(t, Path("./test3"), goBase+"/test3")
	assertTest(t, Path("././/test4"), goBase+"/test4")
}
func TestAppPath(t *testing.T) {
	Setup("testapp")
	goBase := GoBasePath()
	assertTest(t, AppPath(), goBase+"/src/testapp")
}
func TestAppFullPath(t *testing.T) {
	Setup("testapp")
	goBase := GoBasePath()
	assertTest(t, AppFullPath("test1"), goBase+"/src/testapp/test1")
	assertTest(t, AppFullPath("/test2"), goBase+"/src/testapp/test2")
	assertTest(t, AppFullPath("./test3"), goBase+"/src/testapp/test3")
	assertTest(t, AppFullPath(".//././test4"), goBase+"/src/testapp/test4")
}
